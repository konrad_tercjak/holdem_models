package konrad.tercjak.poker.kryoreader

import java.io.File

import konrad.tercjak.util.Benchmark
object Main extends KryoReader {

  def main(args: Array[String]) {
    read()

  }
  def read():Unit = {
    
    Benchmark.time {
      val shutdowns=getShowdowns()

      for (i <- 0 to dirSize("output/stages/")-1) {
        val stages = getStages()
        val seats = getSeats()
        val actions = getActions()
        nextFile()
      }

    }

  }
//  def ls(dirName: String): Array[File] = {
//    new java.io.File(dirName).listFiles.filter(_.getName.endsWith(".avro"))
//  }
  def dirSize(path:String):Int={
  Option(new File(path).list).map(_.filter(_.endsWith(".avro")).size).getOrElse(0)
  }
}
