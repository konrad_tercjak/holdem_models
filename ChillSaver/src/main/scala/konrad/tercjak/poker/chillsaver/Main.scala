package konrad.tercjak.poker.chillsaver

import konrad.tercjak.poker.filereader.FileReader

object Main {
  def main(args: Array[String]) {

    val dir = "input"
    val fileReader = new FileReader(dir, ChillSaver)
  }
}
