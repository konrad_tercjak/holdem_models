package konrad.tercjak.poker.chillsaver
import _root_.java.io.FileOutputStream

import com.twitter.chill._
import konrad.tercjak.poker.filereader.Saver
import konrad.tercjak.poker.models._

import scala.collection.mutable.ListBuffer
//import _root_.java.io.ByteArrayOutputStream
//import java.io.FileOutputStream

object  ChillSaver extends Saver {
  val instantiator = new ScalaKryoInstantiator
  instantiator.setRegistrationRequired(false)
  val kryo = instantiator.newKryo()


  def saveStages(stages:ListBuffer[StageRow]):Saver={
    write("stages",stages)
    this
  }
  def saveSeats(seats:ListBuffer[SeatsRow]):Saver={
    write("seats",seats)
    this
  }


  def saveShowdowns(showdowns:ListBuffer[TurnHands]):Saver={ //ToDo because enum have '8'
    write("showdowns",showdowns)
    this
  }

  def saveActions(actions:ListBuffer[StageActions]):Saver={
    write("actions",actions)
    this
  }

  def saveTables(tables: Array[TableRow]):Saver={
    val fos=new FileOutputStream(s"output/tables.kryo")
    val output = new Output(fos, 4096)
    kryo.writeObject(output, tables)

    this
  }
  def write[T](name:String,data:T): Unit ={
    val fos=new FileOutputStream(s"output/$name/$idFile.kryo")
    val output = new Output(fos, 4096)
    kryo.writeObject(output, data)
  }

}
