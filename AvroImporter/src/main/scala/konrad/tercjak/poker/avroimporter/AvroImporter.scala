package konrad.tercjak.poker.avroimporter

import java.io.File

import com.sksamuel.avro4s.{AvroDataInputStream, AvroInputStream}
import konrad.tercjak.poker.models._

trait AvroImporter extends Importer{
  val dir = "output"

  def getStages():Array[StageRow]={
    val is:AvroDataInputStream[StageRow] = AvroInputStream.data[StageRow](new File(s"$dir/stages/$idFile.avro"))
    val arr= is.iterator.toArray
    is.close()
    arr
  }
  def getSeats():Array[SeatsRow]={
    val is = AvroInputStream.data[SeatsRow](new File(s"$dir/seats/$idFile.avro"))
    val arr= is.iterator.toArray
    is.close()
    arr
  }

  def getShowdowns():Array[TurnHands]={//Todo maybe Map[idStage,Array[PlayerHand]]
//    val schema = AvroSchema[TurnHands]
//
//    val is=AvroInputStream.data[TurnHands](new File(s"$dir/showdowns/$idFile.avro"))
//    val arr = is.iterator.toArray
//    is.close()
//    arr
    val playerHandMock= PlayerHand(0,Array(Card(Rank.A, Suits.SPADES)))
    val mock:Array[TurnHands] =  Array[TurnHands](TurnHands(0L, Array(playerHandMock)))

  //Array(PlayerHand(0.toShort, Array(Card(Rank.A, Suits.SPADES))))))
    mock
  }

  def getActions(): Array[StageActions]={
    val is = AvroInputStream.data[StageActions](new File(s"$dir/actions/$idFile.avro"))
    val arr= is.iterator.toArray
    is.close()
    arr
  }
  def getTables(): Array[TableRow]={
    val is = AvroInputStream.data[TableRow](new File(s"$dir/tables.avro"))
    val arr= is.iterator.toArray
    is.close()
    arr
  }

//  def read[T](is:AvroDataInputStream[T]): Array[T] ={
//    val arr= is.iterator.toArray
//    is.close()
//    arr
//  }
}
