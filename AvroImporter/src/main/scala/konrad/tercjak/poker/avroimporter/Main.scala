package konrad.tercjak.poker.avroimporter

import java.io.File

import com.sksamuel.avro4s.AvroInputStream
import konrad.tercjak.poker.models._
import konrad.tercjak.util.Benchmark

//import konrad.tercjak.poker.filereader.FileReader
object Main extends AvroImporter {
  def main(args: Array[String]) {
    read()

  }
  def read():Unit = {
    import com.sksamuel.avro4s.AvroSchema
    val schema = AvroSchema[TurnHands]
    println(schema)



    Benchmark.time {
      val shutdowns=getShowdowns()

      for (i <- 0 to dirSize("output/stages/")-1) {
        val stages = getStages()
        val seats = getSeats()
        val actions = getActions()
        nextFile()
      }


    }
  }
//  def ls(dirName: String): Array[File] = {
//    new java.io.File(dirName).listFiles.filter(_.getName.endsWith(".avro"))
//  }
  def dirSize(path:String):Int={
  Option(new File(path).list).map(_.filter(_.endsWith(".avro")).size).getOrElse(0)
  }
}
