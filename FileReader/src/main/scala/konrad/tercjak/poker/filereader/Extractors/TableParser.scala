package konrad.tercjak.poker.filereader.Extractors

import konrad.tercjak.poker.filereader.LinesParser
import konrad.tercjak.poker.models._

trait TableParser {
  // implementers to also be a sub-type of LinesParser.

  // This is specially useful when we have more abstract definitions of trait
  // and a more concrete trait is expected for the instances of the class.
  this: LinesParser =>
  def getTable(): Unit = {
   var line = lines.next()
    skipPrefix("Seat")
    skipEmpty()
//    lines.next()
//    lines.next()
  }
}
