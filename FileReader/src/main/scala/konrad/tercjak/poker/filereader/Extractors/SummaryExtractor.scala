package konrad.tercjak.poker.filereader.Extractors

import konrad.tercjak.poker.models._

import scala.collection.mutable.ListBuffer


object SummaryExtractor {
  def getBoard(lines: Iterator[String]):Option[Array[Card]] = {

    var boardCardsR:Option[Array[Card]]=None
    var rakeText = lines.next() //TODO add rake extractor

    var boardText = lines.next()

    if(!boardText.startsWith("Board [")){
      if(rakeText.startsWith("Board [")){
        boardText=rakeText
        boardCardsR= getBoardCards(boardText)
      }
    }
    else {
      boardCardsR= getBoardCards(boardText)
    }
    boardCardsR

  }

    def getBoardCards(boardText:String):Option[Array[Card]]={
      var cards = boardText.substring(7, boardText.size-1).split(' ')
      var boardCards=new Array[Card](cards.length)

      for (i <-0 until cards.length) {
        var (rankStr,suitStr)=cards(i).splitAt(1)
        var rank: Rank = RankFactory.fromFile(cards(i).dropRight(1))
//          withNameExtended(cards(i).dropRight(1))
        var suit: Suits = Suits.fromFile(cards(i).takeRight(1))
        boardCards(i) = Card(rank, suit)
      }
      Option(boardCards)

    }





}