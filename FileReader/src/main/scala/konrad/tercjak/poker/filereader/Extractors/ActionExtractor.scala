package konrad.tercjak.poker.filereader.Extractors
import konrad.tercjak.poker.filereader.ActionPrefix._
//import Rows.ActionInfoRow
import scala.collection.mutable.{ListBuffer,HashMap}
//import konrad.tercjak.poker.filereader.ActionPrefix._
import konrad.tercjak.poker.models.{_}

object ActionExtractor{
  var lastPlayerSitID:Option[Int]=None

//  var turnActions=new PoketCardsHistory

    def get(lines: Iterator[String],
            seatMap: HashMap[ String,Int],
            stageid:Long): Option[StageActions]= {

      var history = new ListBuffer[ActionRow]()

      skipLines("*** ", lines)
      var line = lines.next
      var allActions = new ListBuffer[List[ActionRow]]()

      while (!line.startsWith("*** ")) {
        val hash = line.take(22)
        val playerSitID = getPlayerSitID(hash, seatMap, stageid)
        lastPlayerSitID = Some(playerSitID)


        val actionBet = getAction(line.drop(25))
        history.+=(ActionRow(playerSitID, actionBet._2.toFloat, actionBet._1))
        line = lines.next

      }

      allActions.+=(history.toList)

      val reverseMap = for ((k, v) <- seatMap) yield (v, k)
      var playersHashes: ListBuffer[String] = history.map(x => reverseMap(x.idSeat))

      var lastTurnTxt: String = null
      var turnID = 1
//      PlayersStats.addPlayersTurns(playersHashes, turnID)
//
//      while (!line.startsWith("*** SHOW DOWN ***")) {
//        var t = get2(lines, seatMap, line)
//        turnID += 1 // To do add
//        //add FLOP3 TURN4 RIVER5
//        //        history=t._1
//        //       playersHashes =history.map(x=>reverseMap(x._1))
//        PlayersStats.addPlayersTurns(playersHashes, turnID)
//        allActions.+=(history.toList)
//        lastTurnTxt = line
//        line = t._2
//      }

      allActions.length match {
        case 0 => None
        case _ =>Some(StageActions(stageid,allActions.toList))

      }

    }

  def skipLines(prefix:String,lines: Iterator[String]): String ={
    var line =lines.next
    while (!line.startsWith("*** ")) {
      line=lines.next
    }

    line
  }
  def getPlayerSitID(hash:String,seatMap: HashMap[ String,Int],stageid:Long): Int ={
    seatMap.get(hash) match {
      case Some(value)  => value
      case None         => whenPlayerSitIN(hash,seatMap,stageid)
    }
  }
  def whenPlayerSitIN(hash:String,seatMap: HashMap[ String,Int],stageid:Long): Int ={
    // When player SITIN in last moment ISNT IN seatMap like  in STAGE #3017480090

        var ids=seatMap.values.toIterator
        var id=ids.next
        while (Some(id)!=lastPlayerSitID){ //todo
          id= ids.next()
        }
        var id2=ids.next

        if(Math.abs(id2-id)>1){
          seatMap.put(hash,id+1)
        }
        else {
          throw new NoSuchElementException(stageid.toString+"No such hash= "
            +hash+" in :"+(seatMap.map(x=>x._1)+"/n").mkString )
        }
        id+1
  }

  def get2(lines: Iterator[String],
          seatMap: HashMap[ String,Int], _line:String): Tuple2[ListBuffer[ActionRow],String] = {
    var history = new ListBuffer[ActionRow]()
//  var line=_line
    var line=lines.next
      while (!line.startsWith("*** ")) {
        val hash = line.take(22)
        val playerSitId = seatMap.get(hash) match {
          case Some(value) =>  value
          case None        =>  //println(hash)
                               throw new NoSuchElementException("No playerhash in seatsMap "+hash)
        }

        val actionBet = getAction(line.drop(25))

        history.+=(ActionRow(playerSitId, actionBet._2.toFloat, actionBet._1))
        line = lines.next

      }

    (history,line)
  }

      def getAction(txt:String): (PokerAction,Double) ={

        var betVal:Double=0d
        def setBet(rest:String):Unit=betVal=rest.substring(rest.indexOf("$") + 1).toFloat

        val action:PokerAction = txt match {

          case "Checks" =>            PokerAction.Checks
          case "Folds" =>             PokerAction.Fold

          case  Raise(rest) =>        setBet(rest)
                                      PokerAction.Raise

          case AllInRaise(rest) =>    setBet(rest)
                                      PokerAction.Raise

          case Call(rest) =>          setBet(rest)
                                      PokerAction.Call

          case FoldShow(rest) =>      //TODO Folds (Fold & Show [Ac 2d])
                                      PokerAction.Fold

          case Bets(rest) =>          setBet(rest)
                                      PokerAction.Bets

          case AllIn(rest) =>         setBet(rest)
                                      PokerAction.Raise

          case Returned(rest) =>      PokerAction.Return
          case "Checks (Timeout)" =>  PokerAction.TimeoutChecks
          case "Folds (Timeout)"  =>  PokerAction.TimeoutFold

          case s: String =>           println("errorAction: " + s)
                                      PokerAction.TimeoutChecks
        }

    (action,betVal)
   }

}