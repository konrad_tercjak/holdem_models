package konrad.tercjak.poker.filereader

object ActionPrefix {

   class Prefix(prefix:String){
    def unapply(str:String):Option[String]= str match {
      case s if s.startsWith(prefix) => Some(s.stripPrefix(prefix))
      case _ => None
    }
  }

  val Raise= new Prefix("Raises $")
  val Call = new Prefix("Calls $")
  val AllInRaise = new Prefix("All-In(Raise) $")
  val Returned = new Prefix("returned ($")
  val FoldShow = new Prefix("Folds (Fold & Show [")
  val AllIn = new Prefix("All-In $")
  val Bets= new Prefix("Bets $")


}