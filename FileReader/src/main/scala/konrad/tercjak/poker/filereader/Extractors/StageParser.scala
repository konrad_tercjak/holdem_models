package konrad.tercjak.poker.filereader.Extractors

import java.time._

import konrad.tercjak.poker.models._
import konrad.tercjak.poker.filereader.{LinesParser, TablesBuilder}

import scala.collection.mutable



trait StageParser{
  this: LinesParser =>
  val tableMap =new  mutable.HashMap[String, Int]()

  def getStageRow(  _line:String) :Option[StageRow] ={
//    var stage:Option[StageRowDB]=None

     val line=takePrefix("Stage #",_line)

    var stage:Option[StageRow]=
      if(!line.isEmpty)
     Some(getStage(line.drop(7)))
      else{
      println("#END OF FILE")
      None
    }

    stage
  }
  def getStage(line: String): StageRow = {
//  println(line)
//    case class myStage(stageID:Int,gameType:String,money:String)

    var (id, rest) = line.splitAt(10)
    var (gameType, rest2:String) = rest.splitAt(rest indexOf '$')// todo clean

    var (moneyStr, date) = rest2.splitAt(rest2 indexOf '-')

    var money = moneyStr.tail//.toFloat


    var tableString=  getTable()

    var tableName=tableString(0)
    var dealerID=tableString(1)

//    tableMap.+=(tableName,)

    var tableID=TablesBuilder.getID(tableName,money)


    date= date.substring(2, date.length - 5)
    val dateFormat = """(\d\d\d\d)-(\d\d)-(\d\d)"""
    val timeFormat = """(\d\d):(\d\d):(\d\d)"""
    val dataTime=(dateFormat+" "+timeFormat).r

var dateTimer:LocalDateTime=    date match {
      case dataTime(year, month, day,hour,minutes,seconds) =>
       LocalDateTime.of(
         year.toInt,
         month.toInt,
         day.toInt,
         hour.toInt,
         minutes.toInt,
         seconds.toInt
       )
//      case s:String=> //println("WRONG Date and time :"+ s)

}
    var zoneId = ZoneOffset.UTC
    var epoch = dateTimer.toEpochSecond(zoneId)

    StageRow(id.toLong,tableID,dealerID.toShort, epoch)
  }

 private def getTable(): Array[String] = {
//    println(line)

   var  line=lines.next
    //    DEFIANCE ST (Real Money) Seat #6 is the dealer
    var (name, rest) = line.splitAt((line indexOf ')') + 1)
    var dealerId = rest.substring(7, 8)//.toShort
    name = name.substring(7, (line indexOf '(') - 1)
    Array(name,dealerId)
  }
}