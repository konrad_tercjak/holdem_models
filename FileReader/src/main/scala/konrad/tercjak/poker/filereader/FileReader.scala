package konrad.tercjak.poker.filereader

import java.io.{ByteArrayOutputStream, File, FileOutputStream}
import java.util

import konrad.tercjak.poker.filereader.Extractors._
import konrad.tercjak.poker.models._

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.io.Source
import konrad.tercjak.util.Benchmark
//object PlayerModels extends mutable.HashMap[String,PlayerModel]{
//  def  add(key:String,cards:Array[Card]): Unit ={
//    get(key) match {
//      case Some(model) =>model.addCards(cards(0),cards(1))
//      case None => var model=new PlayerModel
//        model.addCards(cards(0),cards(1))
//        put(key,model)
//    }
//
//  }
//}

  class FileReader(dirName:String,saver:Saver) extends LinesParser with TableParser with StageParser
                                      with ShowdownParser with SeatsParser{
    override var lines =Iterator("").buffered

    def recursiveListFiles(dirName: String): Array[File] = {
      new java.io.File(dirName).listFiles.filter(_.getName.endsWith(".txt"))
    }

    var stages = new ListBuffer[StageRow]()
    var seats = new ListBuffer[SeatsRow]()
    var showdowns = new ListBuffer[TurnHands]()

    var actions =new ListBuffer[StageActions]()
//      new util.TreeMap[Long,List[List[ActionRow]]]()

//    var idFile = 0

//    val files = dirName.list.filter(_.getName.endsWith(".txt"))

    for (file <- recursiveListFiles(dirName)) {
      stages = new ListBuffer[StageRow]()
      seats = new ListBuffer[SeatsRow]()

      showdowns = new ListBuffer[TurnHands]()
      actions = new ListBuffer[StageActions]() //new util.TreeMap[Long,List[List[ActionRow]]]()

      lines= Source.fromFile(file).getLines().buffered
//      idFile += 1
      Benchmark.time(readLines())


      assert(actions.size==1000)
      assert(stages.size==1000)
      assert(seats.size==1000)


      saver.saveStages(stages)
           .saveSeats(seats)
           .saveShowdowns(showdowns)
           .saveActions(actions)
           .nextFile()



      println("allShowdown.length : "+showdowns.size)


    }

   var tables:Array[TableRow]= TablesBuilder.toArray()
    saver.saveTables(tables)

    println("Tables : "+tables.size)

//   tables.foreach(t=>println(t.toString()))

    //    PlayerModel.foreach(x=>{
//      println("\nPlayer: "+x._1+"\n")
//      x._2.normalizeAll()
//      //       println(x._2.unSuited.mkString)
//    })

//   var once= allShowdown.filter(x=>x.exists(y=>y._1=="FEENmRh7HO+E/mtHbgSWhg"))
//    once.foreach(x=>x.foreach(
//     y=> {
//       println(y._1)
//       println(y._2(0).toString() + y._2(1).toString())
//     }
//    ))


//    z.foreach(=>println(id+" "+ cards))

    def readLines():Unit = for {

      line <- lines
      stage <- getStageRow(line)

      idStage = stage.idStage
      _ = stages.prepend(stage)

      (seatsWithCoins, seatMap) <- getSeats(idStage)
      _ = seats.prepend(seatsWithCoins)

      stageActions <- ActionExtractor.get(lines, seatMap,idStage)
      _ = actions.prepend(stageActions)

      showdown <- ShowdownExtractor.getCards(lines,idStage,seatMap)
//     _= showdown.foreach(x=>
//       println(x._1+": "+x._2(0).toString()+x._2(1).toString())
//
//     )

     _= showdowns.prepend(showdown)
//      _ = showdown.foreach{ player =>
//        PlayerModels.add(player., player._2)
//      }

      boardData = SummaryExtractor.getBoard(lines)

      boardCards<-boardData
      table=getTable()
//      _=allTables.append()
    }()


  }

