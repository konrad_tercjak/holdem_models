package konrad.tercjak.poker.filereader

import java.util

import konrad.tercjak.poker.models._

import scala.collection.mutable.ListBuffer

trait Saver{


  type TurnActions=List[ActionRow]

  var idFile=0
  def nextFile():Saver={idFile+=1;this}

  def saveStages(stages:ListBuffer[StageRow]):Saver
  def saveSeats(seats:ListBuffer[SeatsRow]):Saver
  def saveShowdowns(shutdowns:ListBuffer[TurnHands]):Saver

  def saveActions(shutdowns: ListBuffer[StageActions] ):Saver
  def saveTables(tables: Array[TableRow]):Saver

}