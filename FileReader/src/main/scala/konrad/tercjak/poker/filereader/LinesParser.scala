package konrad.tercjak.poker.filereader

import scala.collection.mutable.ListBuffer

abstract class  LinesParser {
  var lines: BufferedIterator[String]//=Iterator("").buffered

  @inline def skipPrefix(prefix:String):Unit = {
    var line=""
    lines.dropWhile(
      _.length>prefix.length&&line.startsWith(prefix)
    )
    // while (line.length>prefix.length&&line.startsWith(prefix)){
    //    line=lines.next()
    // }
  }


  @inline def takePrefix(prefix:String, l:String=""):String = {
    var line=l
//    lines.dropWhile(!(line.startsWith(prefix)))

     while (lines.hasNext&&(!line.startsWith(prefix))){
        line=lines.next()
     }
    line
  }



 @inline def skipEmpty() : Unit ={
    while (lines.head.isEmpty) { lines.next() }
  }
//  def dropTo(predicate: String => Boolean) : Unit = {
//    lines.dropWhile(predicate(_))
//  }

  @inline def whilePrefix [T](prefix:String,l:String=lines.next)(f: String => T):List[T] = {
    var line=l
    val list=new ListBuffer[T]()

    while (line.startsWith(prefix)){
      list+=f(line)
      line=lines.next()
    }
    list.toList
  }

  @inline def whileNotPrefix [T](prefix:String,l:String=lines.next)(f: String => T):List[T] = {
    var line=l
    val list=new ListBuffer[T]()

    while ((!line.startsWith(prefix))){
      print(line)
      list+=f(line)
      line=lines.next()
    }

    list.toList
  }

}
