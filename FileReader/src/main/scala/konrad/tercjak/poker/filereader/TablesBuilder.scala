package konrad.tercjak.poker.filereader

import scala.collection.mutable.{HashMap, ListBuffer}
import  konrad.tercjak.poker.models._

object TablesBuilder{



  val tableData= new HashMap[String, (Int,Float)]()
  val seats =new ListBuffer[Int]()

  var nextID: Int = 0
  var lastID: Int = -1

  def getID(key: String, stakes: String): Int = {
    tableData.get(key) match {
      case Some(value) =>
        lastID = value._1
        assert(value._2==stakes.toFloat)
        lastID
      case None =>
        tableData.put(key, (nextID, stakes.toFloat))
        nextID += 1
        lastID += 1
        lastID
    }

  }
  def getID(playerHash:String): Int = {
    tableData(playerHash)._1
  }

  def setNumOfSits(newNumSeats: Int): Unit = {

    if(seats.length>lastID){
      val currNumSeats:Int = seats(lastID)
      seats(lastID) = Math.max(newNumSeats, currNumSeats)
    }
    else {
      seats.append(newNumSeats)
    }
  }

  def toArray(): Array[TableRow] = {
    var y: Array[TableRow] =  (for {
      (key, value) <- tableData
      numOfSits = seats(value._1)
    } yield  TableRow(value._1, key, value._2, numOfSits))(collection.breakOut)

    y.sortBy(x=>x.idTable)

  }

}
