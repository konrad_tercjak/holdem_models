package konrad.tercjak.poker.protosaver

import java.io.File

import konrad.tercjak.poker.filereader.Saver
import konrad.tercjak.poker.models._

import scala.collection.mutable.ListBuffer

object ProtoSaver extends Saver {

  def saveStages(stages: ListBuffer[StageRow]): Saver = {


    this
  }

  def saveSeats(seats: ListBuffer[SeatsRow]): Saver = {

    this
  }


  def saveShowdowns(showdowns: ListBuffer[TurnHands]): Saver = {
    this
  }

  def saveActions(actions: ListBuffer[StageActions]): Saver = {

    this
  }

  def saveTables(tables: Array[TableRow]): Saver = {

    this
  }
}


