package konrad.tercjak.poker.protosaver

import konrad.tercjak.poker.filereader.FileReader
object Main {
  def main(args: Array[String]) {

    val dir = "input"
    val fileReader = new FileReader(dir, ProtoSaver)
  }
}
