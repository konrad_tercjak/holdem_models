package konrad.tercjak.poker.avrosaver
import java.io.{ByteArrayOutputStream, File, FileOutputStream}
import java.util

import konrad.tercjak.poker.filereader.Saver
import konrad.tercjak.poker.models._

import scala.collection.mutable.ListBuffer
import com.sksamuel.avro4s.{AvroSchema, AvroBinaryOutputStream => BinaryStream, AvroDataOutputStream => DataStream, AvroOutputStream => OutputStream}

object AvroSaver extends Saver {

  def saveStages(stages:ListBuffer[StageRow]):Saver={
    save(stages.toList,
      OutputStream.data[StageRow](
        new File(s"output/stages/$idFile.avro")
      )
    )
    this
  }
  def saveSeats(seats:ListBuffer[SeatsRow]):Saver={
   save(seats.toList,
      OutputStream.data[SeatsRow](
        new File(s"output/seats/$idFile.avro")
      )
    )
    this
  }


  def saveShowdowns(showdowns:ListBuffer[TurnHands]):Saver={ //ToDo because enum have '8'
//   val schema=AvroSchema[TurnHands]
//    val schema=AvroSchema[Card]

//    println(schema)
    save(showdowns.toList,
      OutputStream.data[TurnHands](
        new File(s"output/showdowns/$idFile.avro")
      )
    )

//    val fos=new FileOutputStream(s"output/showdowns/$idFile.avro")
//    val baos = new ByteArrayOutputStream
//
//    save(showdowns.toList,
//      OutputStream.binary[TurnHands](
//        baos
//        // new File(s"output/showdowns/$idFile.avro")
//      )
//    )
//    baos.writeTo(fos)
    this
  }

  def saveActions(actions:ListBuffer[StageActions]):Saver={
    save(actions.toList,
      OutputStream.data[StageActions](
        new File(s"output/actions/$idFile.avro")
      )
    )
    this
  }

//  def save(xs: List[T], value: DataStream[TurnHands]) = ???



  //  def saveTables(shutdowns:ListBuffer[TurnHands]):Saver=this
  def saveTables(tables: Array[TableRow]):Saver={
    save(tables.toList,
      OutputStream.data[TableRow](
        new File("output/tables.avro")
      )
    )
    this
  }

//  def save[T](xs: Array[T], os: DataStream[T]):Unit = {
//    os.write(xs)
//    os.flush()
//    os.close()
//  }

  def save[T](item: T, os: DataStream[T]): Unit = {
    os.write(item)
    os.flush()
    os.close()
  }

  def save[T](xs:List[T],os:DataStream[T]):Unit={

    os.write(xs)
    os.flush()
    os.close()
  }

//  def save[T](item:T,os:BinaryStream[T]):Unit={
//
//    os.write(item)
//    os.flush()
//    os.close()
//  }
//  def save[T](xs:List[T],os:BinaryStream[T]):Unit={
//
//    os.write(xs)
//    os.flush()
//    os.close()
//  }

}
