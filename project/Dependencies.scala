import sbt.{ModuleID, _}

object Dependencies {

  private lazy val version = new {
    val scalaTest       = "3.0.0"
    val scalaCheck      = "1.13.4"
    val uTest           = "0.6.3"
    val fastParse       = "1.0.0"
    val avro4s          = "1.8.0"
    val snakeYaml       = "1.18"
//    val msgpack4s       = "0.6.0"
//    val msgpack         = "0.8.13"
    val chill           = "0.9.2"


  }

  def compile   (deps: ModuleID*): Seq[ModuleID] = deps map (_ % Compile)
  def provided  (deps: ModuleID*): Seq[ModuleID] = deps map (_ % Provided)
  def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")
  def runtime   (deps: ModuleID*): Seq[ModuleID] = deps map (_ % Runtime )
  def container (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "container" )


  lazy val library = new  {
//    val test  = "org.scalatest" %% "scalatest" % version.scalaTest
    val check     = "org.scalacheck" %% "scalacheck" % version.scalaCheck
    val uTest     = "com.lihaoyi" %% "utest" % version.uTest
    val fastParse = "com.lihaoyi" %% "fastparse" % version.fastParse
    val avro4s    = "com.sksamuel.avro4s" %%  "avro4s-core" % version.avro4s
    val snakeYaml = "org.yaml" % "snakeyaml" %version.snakeYaml
//    val msgpack4s = "org.velvia" %% "msgpack4s" % version.msgpack4s
//    val msgpack4s   = "org.msgpack" % "msgpack-scala_2.12" %version.msgpack
//    val msgpack   = "org.msgpack" % "msgpack-core" %version.msgpack
    val chill="com.twitter" %% "chill" % version.chill

  }


  val testDependiencies = test(library.check,library.uTest)


//  val module1Dependencies: Seq[ModuleID] = Seq(
//    library.uTest,
//    library.check
//  )
//
//  val module2Dependencies: Seq[ModuleID] = Seq(
//    library.test,
//    library.check
//  )

//  val myappDependencies: Seq[ModuleID] = Seq(
//    library.test,
//    library.check
//  )

//  val ansviaCommons = "com.ansvia" % "ansvia-commons" % "0.1.8-20140421-SNAPSHOT"
//  val specs2        = "org.specs2" %%  "specs2" % "1.12.4"
//  val logback       = "ch.qos.logback" % "logback-classic" % "1.0.9"

//  "com.lihaoyi" %% "utest" % "0.6.3" %


}
