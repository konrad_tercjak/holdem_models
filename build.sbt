import Dependencies.{compile, library, testDependiencies}
//import sbt.Keys.testFrameworks

name := "holdemtools"

//version := "0.1"
//
//scalaVersion := "2.12.4"

//PB.targets in Compile := Seq(
//  scalapb.gen() -> (sourceManaged in Compile).value
//)

lazy val commonSettings = Seq(
  organization := "konrad.tercjak",
  version := "0.1.0-SNAPSHOT",
//  scalaVersion := "2.12.4",
  testFrameworks += new TestFramework("utest.runner.Framework"),
  resolvers ++= Seq(
   Resolver.bintrayIvyRepo("scalacenter", "sbt-releases"),

"softprops-maven" at "http://dl.bintray.com/content/softprops/maven",
    "Sonatype OSS Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/",
    "Atlassian Releases" at "https://maven.atlassian.com/public/",
    "Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases/",
    "Central" at "http://central.maven.org/maven2/",
    "central" at "http://repo1.maven.org/maven2",
    "Spring" at "http://repo.spring.io/plugins-release/",
    "Sun Maven2 Repo" at "http://download.java.net/maven/2"
  ),libraryDependencies ++= testDependiencies,

  initialCommands := "import konrad.tercjak.poker._",

)

lazy val root = (project in file("."))
  .aggregate(util, fileReader)
  .settings(
    commonSettings,
    update / aggregate := false,
    //    libraryDependencies ++=
//      compile(ansviaCommons) ++
//      runtime(logback)

  )
lazy val avroReader = (project in file("AvroImporter"))
  .dependsOn(pokerModels,util)
  .settings(
    commonSettings,
    name := "AvroImporter",
    moduleName := "AvroImporter",
    libraryDependencies ++=compile(library.avro4s)

    // other settings
  )
lazy val kryoReader = (project in file("KryoReader"))
  .dependsOn(pokerModels,util)
  .settings(
    commonSettings,
    name := "KryoReader",
    moduleName := "KryoReader",
    libraryDependencies ++=compile(library.chill)
  )
lazy val kryoImporter = Project( id = "kryoImporter",
                                base = file("kryoImporter"))
  .dependsOn(pokerModels,util)
  .settings(
    commonSettings,
    name := "kryoImporter",
    moduleName := "kryoImporter",
    libraryDependencies ++=compile(library.chill)
  )
lazy val avroSaver = (project in file("AvroSaver"))
  .dependsOn(pokerModels,util,fileReader)
  .settings(
    commonSettings,
    name := "AvroSaver",
    moduleName := "AvroSaver",
    libraryDependencies ++=compile(library.avro4s)

    // other settings
  )

lazy val chillSaver = (project in file("ChillSaver"))
  .dependsOn(pokerModels,util,fileReader)
  .settings(
    commonSettings,
    name := "ChillSaver",
    moduleName := "ChillSaver",
    libraryDependencies ++=compile(library.chill)

    // other settings
  )

lazy val protoSaver = (project in file("protoSaver"))
  .dependsOn(pokerModels,util,fileReader)
  .settings(
    commonSettings,
    name := "ProtoSaver",
    moduleName := "ProtoSaver",
//    libraryDependencies ++=compile(library.avro4s)

    // other settings
  )

lazy val yamlSaver = (project in file("YamlSaver"))
  .dependsOn(pokerModels,util,fileReader)
  .settings(
    commonSettings,
    name := "YamlSaver",
    moduleName := "YamlSaver",
    libraryDependencies ++=compile(library.snakeYaml)

    // other settings
  )

lazy val fileReader = (project in file("FileReader"))
  .dependsOn(pokerModels,util)
  .settings(
    commonSettings,
    name := "FileReader",
    moduleName := "FileReader",
    libraryDependencies ++=compile(library.fastParse)
    // other settings
  )

lazy val pokerModels = (project in file("pokerModels"))
  .settings(
    commonSettings,
    name := "PokerModels",
    moduleName := "PokerModels",
//    libraryDependencies ++=compile(library.fastParse)
    // other settings
  )


lazy val util = (project in file("util"))
  .settings(
    commonSettings,
    name := "util",
    moduleName := "Util",

    // other settings
  )

//mainClass in (Compile, run) := Some("konrad.tercjak.poker.Main")
//enablePlugins(SbtIdeaPlugin)