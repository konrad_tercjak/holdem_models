package konrad.tercjak.poker.kryoreader

import _root_.java.io.FileInputStream

import com.twitter.chill.{Input, ScalaKryoInstantiator}
import konrad.tercjak.poker.models._

trait KryoReader extends Importer{
  val dir = "output"

  val instantiator = new ScalaKryoInstantiator
  instantiator.setRegistrationRequired(false)
  val kryo = instantiator.newKryo()

  def getStages():Array[StageRow]={
    val name= "stages"
    val input=new Input(new FileInputStream(s"output/$name/$idFile.kryo"))

    val deser = kryo.readObject(input, classOf[Array[StageRow]])
    deser
  }

  def getSeats():Array[SeatsRow]={
    var name= "seats"

    val input=new Input(new FileInputStream(s"output/$name/$idFile.kryo"))
    val deser = kryo.readObject(input, classOf[Array[SeatsRow]])
    deser
  }

  def getShowdowns():Array[TurnHands]={//Todo maybe Map[idStage,Array[PlayerHand]]
    var name= "showdowns"

    val input=new Input(new FileInputStream(s"output/$name/$idFile.kryo"))
    val deser = kryo.readObject(input, classOf[Array[TurnHands]])
    deser
  }

  def getActions(): Array[StageActions]={
    var name= "actions"

    val input=new Input(new FileInputStream(s"output/$name/$idFile.kryo"))
    val deser = kryo.readObject(input, classOf[Array[StageActions]])
    deser
  }
//    read("actions")


  def getTables(): Array[TableRow]={
    val input=new Input(new FileInputStream(s"output/tables.kryo"))

    val deser = kryo.readObject(input, classOf[Array[TableRow]])
    deser
  }


def read[T](name:String): Array[T] ={
  val input=new Input(new FileInputStream(s"output/$name/$idFile.kryo"))

  val deser = kryo.readObject(input, classOf[Array[T]])
//  deser.asInstanceOf(Array[T])
  deser
}

}
