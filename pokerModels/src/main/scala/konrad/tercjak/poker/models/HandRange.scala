package konrad.tercjak.poker.models

object isIn {
  def range45toTJ(lowRank :Rank,highRank:Rank )(): Boolean = {
    highRank.ordinal() == lowRank.ordinal() + 1 &&
      (lowRank.ordinal() > 3) &&
      (highRank.ordinal() < 10)
  }
  def range35toTQ(lowRank :Rank,highRank:Rank )(): Boolean = {
    highRank.ordinal() == lowRank.ordinal() + 2 &&
      (lowRank.ordinal() > 3) &&
      (highRank.ordinal() < 11)
  }
  def range25toTK(lowRank :Rank,highRank:Rank )(): Boolean = {
    highRank.ordinal() == lowRank.ordinal() + 2 &&
      (lowRank.ordinal() > 3) &&
      (highRank.ordinal() < 11)
  }
  def rangeA5toAT(lowRank :Rank,highRank:Rank )(): Boolean = {
    highRank.ordinal() == lowRank.ordinal() + 2 &&
      (lowRank.ordinal() > 3) &&
      (highRank.ordinal() < 11)
  }
}
