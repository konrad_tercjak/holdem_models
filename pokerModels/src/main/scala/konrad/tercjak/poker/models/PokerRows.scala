package konrad.tercjak.poker.models


import scala.beans.BeanProperty


case class StageRow(@BeanProperty var idStage:Long,
                    @BeanProperty var idTable: Int,
                    @BeanProperty var idDealer: Int,
                    @BeanProperty var time: Long) //date:Date



case class SeatsRow( @BeanProperty var idStage: Long,
                    @BeanProperty var playerHash: Array[String],
                    @BeanProperty var sitsID: Array[Int])

case class ActionRow(@BeanProperty var idSeat: Int,
                     @BeanProperty var stake: Float,
                     @BeanProperty var action: PokerAction)

//case class TableRowDB( idTable: Int,
//                      tableName: String,
//                      numOfSits: Int,
//                      stakes: Float)

case class TableRow(@BeanProperty var idTable:Int,
                    @BeanProperty var name:String,
                    @BeanProperty var stakes:Float,
                    @BeanProperty var maxSeats:Int){
  override def toString: String = s"#$idTable '$name' Stake: $stakes $$, Max seats: $maxSeats"
}

case class StageActions(@BeanProperty var idStage: Long,history:List[List[ActionRow]])