package konrad.tercjak.poker.models

import konrad.tercjak.poker.models.Rank._

object RankFactory {
  def fromFile(str:String): Rank= str match{
    case "2"  => TWO
    case "3"  => THREE
    case "4"  => FOUR
    case "5"  => FIVE
    case "6"  => SIX
    case "7"  => SEVEN
    case "8"  => EIGHT
    case "9"  => NINE
    case "10" => T
    case "J" => J
    case "Q" => Q
    case "K" => K
    case "A" => A
  }
}
