package konrad.tercjak.poker.models

import scala.beans.BeanProperty
import scala.collection.mutable

sealed abstract class Ranks(@BeanProperty val id:Byte)extends Comparable[Ranks]{//,val txt:String avro
  override  def toString(): String = id match {
    case 0 => "2"
    case 1 => "3"
    case 2 => "4"
    case 3 => "5"
    case 4 => "6"
    case 5 => "7"
    case 6 => "8"
    case 7 => "9"
    case 8 => "T"
    case 9 => "J"
    case 10 => "Q"
    case 11 => "K"
    case 12 => "A"
    case 13 => "?"
  }
   def >(x: Ranks): Boolean = this.id-x.id>0
   def <(x: Ranks): Boolean = this.id-x.id<0
  override def compareTo(x:Ranks):Int={
    this.id-x.id
  }
   def maxMin(x:Ranks): (Byte,Byte) ={
     if (this.id<x.id) (x.id,this.id)
     else (this.id,x.id)
   }


}

//konrad.tercjak.poker.models.enum class Ranks(ch:String){
//  override  def toString(): String =ch
//}

object Ranks{
  def apply(i: Int) = i match {
    case 0 => TWO
    case 1 => THREE
    case 2 => FOUR
    case 3 => FIVE
    case 4 => SIX
    case 5 => SEVEN
    case 6 => EIGHT
    case 7 => NINE
    case 8 => T
    case 9 => J
    case 10 => Q
    case 11 => K
    case 12 => A
    case 13 => NONE
  }

  case object  TWO extends Ranks(0)
  case object THREE extends Ranks(1)
  case object FOUR extends Ranks(2)
  case object FIVE  extends Ranks(3)
  case object SIX extends Ranks(4)
  case object SEVEN  extends Ranks(5)
  case object EIGHT extends Ranks(6)
  case object NINE extends Ranks(7)
  case object T extends Ranks(8)
  case object J extends Ranks(9)
  case object Q extends Ranks(10)
  case object K extends Ranks(11)
  case object A extends Ranks(12)
  case object NONE extends Ranks(13)


//  val arr:Array[Ranks.type ]= Array(  TWO,
//            THREE,
//            FOUR,
//            FIVE)
//            SIX,SEVEN,EIGHT,NINE,T,J,Q,K,A,None)

  def fromFile(str:String): Ranks= str match{
      case "2"  => TWO
      case "3"  => THREE
      case "4"  => FOUR
      case "5"  => FIVE
      case "6"  => SIX
      case "7"  => SEVEN
      case "8"  => EIGHT
      case "9"  => NINE
      case "10" => T
      case "J" => J
      case "Q" => Q
      case "K" => K
      case "A" => A
 }

//  def fromInt(key:Int): Ranks =key match{
//    case 1  => TWO
//    case 2  => THREE
//    case 3  => FOUR
//    case 4  => FIVE
//    case 5  => SIX
//    case 6  => SEVEN
//    case 7  => EIGHT
//    case 8  => NINE
//    case 9 => T
//    case 10 => J
//    case 11 => Q
//    case 12 => K
//    case 13 => A
//    case 14 => NONE
//  }

}






