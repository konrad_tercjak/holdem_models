package konrad.tercjak.poker.models

import scala.beans.BeanProperty

sealed abstract class Suits(@BeanProperty val id:Int,val txt:String){//extends Comparable[Ranks]{
//override  def toString(): String = symbol
}

object Suits {
  case object HEARTS extends Suits(1,"h")//"❤"
  case object DIAMONDS extends Suits(2,"d")// "♦"
  case object SPADES extends Suits(3,"s" )//"♠"
  case object CLUBS extends Suits(4,"c")//"♣"

  val arr=Array(HEARTS,DIAMONDS,SPADES,CLUBS)

  def fromFile(txt:String):Suits=txt match{
    case "h"=>HEARTS
    case "d"=>DIAMONDS
    case "s"=>SPADES
    case "c"=>CLUBS
  }
}