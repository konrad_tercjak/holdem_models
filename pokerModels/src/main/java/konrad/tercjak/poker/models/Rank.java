package konrad.tercjak.poker.models;
import static konrad.tercjak.poker.models.Rank.*;
public enum Rank {
    TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, T, J, Q, K, A;

}