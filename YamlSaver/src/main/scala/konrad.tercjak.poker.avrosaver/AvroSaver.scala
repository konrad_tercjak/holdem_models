package konrad.tercjak.poker.yamlsaver

import java.io.{File, PrintWriter}
import java.util

import konrad.tercjak.poker.filereader.Saver
import konrad.tercjak.poker.models._
import org.yaml.snakeyaml.nodes.Tag
import org.yaml.snakeyaml.representer.Representer
import org.yaml.snakeyaml.{DumperOptions, Yaml}

import scala.beans.BeanProperty
import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer

object AvroSaver extends Saver {
  val options = new DumperOptions
  options.setWidth(1000)
  options.setPrettyFlow(true)
  var yaml = new Yaml(options)

  def saveStages(stages:ListBuffer[StageRow]):Saver={
    val representer = new Representer
    representer.addClassTag(classOf[StageRow], new Tag("!Stage"))
    yaml=new Yaml(representer,options)
    val output : String = yaml.dump(stages.asJava)


    new PrintWriter(s"output/stages/$idFile.yaml") {
      write(output)
      close
    }

    this
  }
  def saveSeats(seats:ListBuffer[SeatsRow]):Saver={
    val output : String = yaml.dump(seats)

    new PrintWriter("seats.yaml") {
      write(output)
      close
    }

    this
  }


  def saveShowdowns(showdowns:ListBuffer[TurnHands]):Saver={ //ToDo
    val representer = new Representer
    representer.addClassTag(classOf[PlayerHand], new Tag("!PlayerHand"))
    representer.addClassTag(classOf[TurnHands], new Tag("!TurnHands"))

    yaml=new Yaml(representer,options)
    val output : String = yaml.dump(showdowns.asJava)


    new PrintWriter(s"output/showdowns/$idFile.yaml") {
      write(output)
      close
    }
    this
  }

  def saveActions(shutdowns:util.TreeMap[Long,List[TurnActions]]):Saver={
    val representer = new Representer
    representer.addClassTag(classOf[ActionRow], new Tag("!ActionInfoRow"))
    yaml=new Yaml(representer,options)

    case class Actions(@BeanProperty var key:Long,@BeanProperty var turnList:Array[ActionRow])

    var myAction:Array[Actions]= shutdowns.asScala.map(x=>
      Actions(x._1,x._2.apply(0).toArray)
    )(collection.breakOut)

    val output : String = yaml.dump(myAction)

    new PrintWriter(s"output/actions/$idFile.yaml") {
      write(output)
      close
    }
    this
  }
  //  def saveTables(shutdowns:ListBuffer[TurnHands]):Saver=this
  def saveTables(tables: Array[TableRow]):Saver={
    import org.yaml.snakeyaml.representer.Representer
    val representer = new Representer

    representer.addClassTag(classOf[TableRow], new Tag("!Table"))
    yaml=new Yaml(representer,options)

    val output : String = yaml.dump(tables)
    //    val output=table.toYaml(tableFormat).prettyPrint
    new PrintWriter("output/table.yaml") {
      write(output)
      close
    }

    this
  }

//  def save[T](item: T, os: DataStream[T]): Unit = {
//    os.write(item)
//    os.flush()
//    os.close()
//  }
//
//  def save[T](xs:List[T],os:DataStream[T]):Unit={
//
//    os.write(xs)
//    os.flush()
//    os.close()
//  }
//
//  def save[T](item:T,os:BinaryStream[T]):Unit={
//
//    os.write(item)
//    os.flush()
//    os.close()
//  }
//  def save[T](xs:List[T],os:BinaryStream[T]):Unit={
//
//    os.write(xs)
//    os.flush()
//    os.close()
//  }

}
