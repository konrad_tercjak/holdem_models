package konrad.tercjak.util
import Birds._

object Logger {
  def logging[A](s: String, x: A) = kestrel(x){ y => println(s + ": " + y) }

}
