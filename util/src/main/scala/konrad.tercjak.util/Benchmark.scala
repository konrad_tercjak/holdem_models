package konrad.tercjak.util
import Birds._

object Benchmark {
  def time[R](block: => R) : R = {
    var t0 = System.nanoTime()
    var result=block
    var t1 = System.nanoTime()

    var d=(t1-t0)/Math.pow(10,9)
    println("Time: "+d+" s")
    result
  }



  def getTime[R](block: =>R):Double={
    var t0 = System.nanoTime()
    var result=block
    var t1 = System.nanoTime()

    var d=(t1-t0)/Math.pow(10,9)
    d
  }

}
